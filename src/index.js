import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import 'bootstrap/dist/css/bootstrap.min.css';

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";

class UpdateModal extends React.Component
{
	constructor(props)
	{
		super(props);
		this.handleClose = this.handleClose.bind(this);
	}

	handleClose()
	{
		this.props.onClose();
	}

	render()
	{
		console.log("Modal : " + this.props.show);
		var me = this;
		var rec = Object.assign({}, this.props.data);
		return (
			<>
			  <Modal show={this.props.show} onHide={this.handleClose}>
				<Modal.Header closeButton>
				  <Modal.Title>Modal heading</Modal.Title>
				</Modal.Header>
				<Modal.Body>

					<Form action="http://localhost:8080/upload" method="post" encType="multipart/form-data">
					  <Form.Group>
						<Form.Label>Product No</Form.Label>
						<Form.Control type="text" name="prdno" placeholder="Name" defaultValue={rec.prdno} />
					  </Form.Group>
					  <Form.Group>
						<Form.Label>Product Name</Form.Label>
						<Form.Control type="text" name="prdnm" placeholder="Name" defaultValue={rec.prdnm} />
					  </Form.Group>
					  <Form.Group>
						<Form.Label>Description</Form.Label>
						<Form.Control as="textarea" name="htmldetail" rows="3" placeholder="Desc" defaultValue={rec.htmldetail} />
					  </Form.Group>
					  <Form.Group>
						<Form.Label>Price</Form.Label>
						<Form.Control type="text" name="selprc" placeholder="Price" defaultValue={rec.selprc} />
					  </Form.Group>
					  <Form.Group>
						<Form.Label>Image</Form.Label>
						<Form.Control type="text" name="prdimage01" placeholder="Name" defaultValue={rec.prdimage01} />
						<input type="file" name="file"/>
					  </Form.Group>
					  <Button variant="primary" type="submit">
						Submit
					  </Button>
					</Form>

				</Modal.Body>
				<Modal.Footer>
				  <Button variant="secondary" onClick={this.handleClose}>
					Close
				  </Button>
				</Modal.Footer>
			  </Modal>
			</>
		  );
	}
}

class Test extends React.Component
{
	constructor(props)
	{
		super(props);

		this.state =
		{
			data: null,
			curData: {},
			updating: false
		};
	}

	componentDidMount()
	{
		this.refresh();
	}

	refresh(sync)
	{
		const options =
		{
			method: "GET"
		};

		if(!sync) sync = false;

		var me = this;
		fetch("http://localhost:8080/fetch/" + sync, options)
			.then(function(response)
			{
				return response.json();
			})
			.then(function(data)
			{
				console.log("data received...");
				console.log(data);
				me.setState(
				{
					data: data
				});
			});
	}

	deleteItem(data)
	{
		console.log(data);
		var me = this;
		const options =
		{
			method: "POST",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify({"prdno": data.prdno})
		};

		fetch("http://localhost:8080/delete", options)
			.then(function(response)
			{
				me.refresh();
			});
	}

	updateItem(data)
	{
		this.setState(
		{
			curData: data,
			updating: true
		});
	}

	onUpdateFinished()
	{
		console.log("onUpdateFinished");
		this.setState(
		{
			updating: false
		});
	}

	drawCard(data)
	{
		var me = this;
		return <Card style={{ width: "18rem" }}>
				<Card.Img variant="top" src={data.prdimage01} />
				<Card.Body>
					<Card.Title>{data.prdnm}</Card.Title>
					<Card.Text>
						{data.htmldetail}
					</Card.Text>
					<Card.Text>
						{"Rp. " + data.selprc}
					</Card.Text>
					<Button variant="primary" onClick={function() { me.updateItem(data); }}>Update</Button>
					<Button variant="secondary" onClick={function() { me.deleteItem(data); }}>Delete</Button>
				</Card.Body>
			</Card>
	}

	drawContainer()
	{
		var rows = [];
		var cols = [];
		const data = this.state.data;
		if(data == null) return null;
		const len = data.length;
		var rowId = -1;
		for(var i=0; i<len; i++)
		{
			if(i % 3 == 0)
			{
				cols = [];
				rowId ++;
				rows[rowId] = <Row key={rowId.toString()}>{cols}</Row>;
			}

			cols[cols.length] = <Col key={data[i].prdno.toString()}>{this.drawCard(data[i])}</Col>;
		}
			
		return rows;
	}

	renderModal()
	{
		var me = this;
		console.log(this.state.updating);
		return (
			<UpdateModal show={this.state.updating} data={this.state.curData} onClose={function() { me.onUpdateFinished(); } }/>
		);
	}

	render()
	{
		var me = this;
		return (
			<>
				{this.renderModal()}
				<Container>
					<Button variant="primary" onClick={function() { me.refresh(true); } }>Fetch Data from elevenia</Button>
					{this.drawContainer()}
				</Container>
			</>
		);
	}
}

ReactDOM.render(
	<Test />,
	document.getElementById("root")
);
